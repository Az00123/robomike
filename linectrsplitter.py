from datetime import date, timedelta, datetime, time
import pandas as pd
import MySQLdb as mdb
import os
import json
import requests
import ast
import numpy as np
import io
import copy


class FusionAPI:
    """To make doing API stuff easy later on"""
    def __init__(self):
        url = 'https://api.strikead.com/v1.1/login'

        with open('login.json') as file:
            login = json.load(file)
        file.close()

        r = requests.post(url, json=login)

        self.token = r.json()['token']

    def get_camp(self, campaign):
        url = "".join(['https://api.strikead.com/v1.1/campaigns/', str(campaign)])
        headers = {"Content-Type": "application/json", "X-Authorization": self.token}
        r = requests.get(url, headers=headers)

        return r.json()

    def put_camp(self, campaign, camp_json):
        url = "".join(['https://api.strikead.com/v1.1/campaigns/', str(campaign)])
        headers = {"Content-Type": "application/json", "X-Authorization": self.token}
        r = requests.put(url, headers=headers, data = json.dumps(camp_json))
        print url
        print r
        return r


def create_click_lines(campaign, camp_json):
    """Treat line items as a set, create child lines copying targeting but adding granular whitelists"""
    def remove_blacklist_pmp_lines(df, camp_json):
        for line in camp_json['line_items']:
            try:
                if not line['targeting']['site_app_ids']['include']:
                    df.drop(df[df.line_item_id == str(line['id'])].index, inplace = True)
                    print 'removed' + str(line['id'])
                elif line['buying_type'] == 'PMP':
                    df.drop(df[df.line_item_id == str(line['id'])].index, inplace = True)
                    print 'removed' + str(line['id'])
            except:
                pass
        return df


    def get_gbq_ctr_data(campaign, camp_json):
        q_gbq = 'SELECT ad_id as line_item_id, sum(impressions) as imps, sum(clicks) as clicks, site_id FROM '

        query_tables = []
        for k in range(1,5):
            query_tables.append("".join(['[strikead.com:strikeadbidderhosting:fusion_production.slicer_', date.strftime(date.today() - timedelta(k), '%Y_%m_%d'), ']']))

        q = q_gbq + ", ".join(query_tables) + ' WHERE campaign_id = ' + "".join(['"', campaign['ID'], '"']) + ' GROUP BY line_item_id, site_id'

        projID = 'strikead.com:strikeadbidderhosting'

        df = pd.read_gbq(q, projID)
        return df


    def create_children(buckets, base):
        children = []
        for bucket in buckets:
            if buckets[bucket].empty:
                pass
            else:
                child_line = copy.deepcopy(base)
                child_line['targeting']['site_app_ids'] = {u'include': True, u'items': buckets[bucket].tolist()}
                child_line['name'] = base['name'] + ' -- ' + bucket
                children.append(child_line)


        count = len(children)
        if count > 0:
            for child in children:
                if child['budget'] != None:
                    child['budget'] = child['budget'] / (float(count) + 1)
                child['daily_cap'] = child['daily_cap'] / (float(count) + 1)
        return children


    df = get_gbq_ctr_data(campaign, camp_json)
    df['CTR'] = df.clicks / df.imps
    df.fillna(0, inplace = True)
    df = remove_blacklist_pmp_lines(df, camp_json)
    new_camp_json = copy.deepcopy(camp_json)
    if not os.path.exists('camp_json_logs'):
        os.makedirs('camp_json_logs')
    with open("".join(['camp_json_logs/', campaign['ID'], '-', date.strftime(datetime.now(), '%Y_%m_%d--%H:%M'), '.txt']), 'w') as outfile:
        json.dump(camp_json, outfile)
    new_line_items = []

    for line in df.line_item_id.unique():
            mean = df[df.line_item_id == line].CTR.mean()
            std = df[df.line_item_id == line].CTR.std()
            high_ctr = df[(df.line_item_id == line) & (df.CTR > mean + std)].site_id
            mid_ctr = df[(df.line_item_id == line) & (df.CTR < mean + std) & (df.CTR > mean)].site_id
            low_ctr = df[(df.line_item_id == line) & (df.CTR < mean) & (df.CTR > 0)].site_id

            for p in range(len(new_camp_json['line_items'])):
                print p
                if new_camp_json['line_items'][p]['id'] == int(line):
                    line_index = p
                    base = copy.deepcopy(new_camp_json['line_items'][p])

            del base['id']

            buckets = {'high_ctr': high_ctr, 'mid_ctr': mid_ctr, 'low_ctr': low_ctr}
            line_set = create_children(buckets, base)
            if new_camp_json['line_items'][line_index]['budget'] != None:
                new_camp_json['line_items'][line_index]['budget'] = new_camp_json['line_items'][line_index]['budget'] / (float(len(line_set)) + 1)
            new_camp_json['line_items'][line_index]['daily_cap'] = new_camp_json['line_items'][line_index]['daily_cap'] / (float(len(line_set)) + 1)
            for child in line_set:
                new_line_items.append(child)

    for i in new_line_items:
        new_camp_json['line_items'].append(i)

    new_camp_json['description'] = new_camp_json['description'] + '\n robomike ctr lines last created on ' + date.strftime(date.today(), '%Y_%m_%d')

    return new_camp_json


def main():
        #### Create new click lines from optimised JSON

    global api
    api = FusionAPI()

    old_json = api.get_camp(campaign['ID'])

    new_json = create_click_lines(campaign, old_json)