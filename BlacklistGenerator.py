from datetime import date, timedelta, datetime, time
import pandas as pd
import MySQLdb as mdb
import os
import json
import requests
import ast
import numpy as np
import smtplib
from email.mime.text import MIMEText
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication

abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)


def make_table_name(date):
        """function to make GBQ table name from date"""
        d = date.strftime('%Y_%m_%d')
        table_name = "".join(['[fusion_production.slicer_', d, ']'])
        return table_name


def get_gbq_data():
    """Get site/app data from GBQ for mtd - 1"""
    # q_gbq = 'SELECT site_id AS id, site_domain as domain, site_or_app_name as name FROM '
    #
    # end = date.today() - timedelta(1)
    # start = end.replace(day = 1)
    # s = start
    #
    # tables = []
    #
    # while s != date.today():
    #     tables.append(make_table_name(s))
    #     s += timedelta(1)
    #
    # query_tables = ", ".join(tables)
    #
    # q = q_gbq + query_tables
    #
    # projID = 'strikead.com:strikeadbidderhosting'
    #
    # print q
    #
    # df = pd.read_gbq(q, projID)
    #
    # return df

    q = "".join(['SELECT site_id as id, site_domain as domain, site_or_app_name as name FROM ', make_table_name(date.today() - timedelta(1)), 'GROUP BY id, domain, name'])

    projID = 'strikead.com:strikeadbidderhosting'
    df = pd.read_gbq(q, projID)

    return df


def send_mail(df_flagged):
    message = MIMEMultipart()
    message['From'] = 'azeem.iqbal@strikead.com'
    message['To'] = "PBUSupply@sizmek.com"
    message['Subject'] = "Flagged Inventory"

    # if date.today().day != 2:
    #     return

    # with open('email.html', 'rb') as template:
    #     message_text = ''.join(template.readlines()).replace('\n', '')
    #     message_text = message_text.format(
    #         date = yday,
    #         Top10_MTD = Top10_MTD_html, Top10_yday = Top10_yday_html)
    # message.attach(MIMEText(message_text, 'html'))

    message_text = 'See CSV'
    message.attach(MIMEText(message_text, 'html'))

    # imps_attachment = 'imps.png'
    # fp = open(imps_attachment, 'rb')
    # img1 = MIMEImage(fp.read())
    # fp.close()
    # img1.add_header('Content-ID', imps_attachment)
    # message.attach(img1)
    #
    # spend_attachment = 'spend.png'
    # fp = open(spend_attachment, 'rb')
    # img2 = MIMEImage(fp.read())
    # fp.close()
    # img2.add_header('Content-ID', spend_attachment)
    # message.attach(img2)
    #
    # imps_country_attachment = 'imps_country.png'
    # fp = open(imps_country_attachment, 'rb')
    # img3 = MIMEImage(fp.read())
    # fp.close()
    # img3.add_header('Content-ID', imps_country_attachment)
    # message.attach(img3)
    #
    # spend_country_attachment = 'spend_country.png'
    # fp = open(spend_country_attachment, 'rb')
    # img4 = MIMEImage(fp.read())
    # fp.close()
    # img4.add_header('Content-ID', spend_country_attachment)
    # message.attach(img4)

    message.attach(MIMEApplication(
    open('flag.csv', 'rb').read(),
    Content_Disposition = 'flagged_inventory.csv',
    Name='flagged_inventory.csv'
    ))



    try:
        server = smtplib.SMTP("smtp.gmail.com", 587)
        server.ehlo()
        server.starttls()
        server.login('azeem.iqbal@strikead.com', 'thisisapassword')
        server.sendmail('azeem.iqbal@strikead.com', "PBUSupply@sizmek.com", message.as_string())

        server.close()
    except:
        print "failed to send mail"


def main():
    """Grab site/app names, IDs for past month, scan for keywords and output csv"""

    df = get_gbq_data()
    df.fillna('0', inplace = True)

    keywords = ['grindr', 'growlr', 'boyahoy', 'wapo', 'brenda', 'scruff', 'skout', 'meetme', 'tagged', "jack'd", 'planetromeo',
                'topface', 'bender', 'qeep', 'hornet', 'tchatche', 'wapa', 'dating', 'porn', 'cybermen', 'zoosk', 'kamasutra',
                'w-match', 'sexy', 'erotic']

    df_brand_safe = None
    for k in keywords:
        temp_df_app = df[df.name.str.lower().str.contains(k)]
        temp_df_web = df[df.domain.str.lower().str.contains(k)]
        temp_df = pd.concat([temp_df_app, temp_df_web])
        df_brand_safe = pd.concat([df_brand_safe, temp_df])

    df_brand_safe_old = pd.read_csv('brand_safe_blacklist.csv')
    df_brand_safe = pd.concat([df_brand_safe, df_brand_safe_old])
    df_brand_safe.drop_duplicates(inplace = True)
    df_brand_safe.to_csv('brand_safe_blacklist.csv', index = False, encoding = 'UTF-8')


    kw2 = ['porn', 'violent', 'sexy', 'erotic']

    df_flagged = None
    for j in kw2:
        temp_df_app = df[df.name.str.lower().str.contains(j)]
        temp_df_web = df[df.domain.str.lower().str.contains(j)]
        temp_df = pd.concat([temp_df_app, temp_df_web])
        df_flagged = pd.concat([df_flagged, temp_df])

    df_flagged_old = pd.read_csv('flag.csv')
    df_flagged.to_csv('flag.csv', index = False, encoding = 'UTF-8')

    send_mail(df_flagged)



if __name__ == '__main__':
    main()