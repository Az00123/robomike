#!/usr/bin/env python

"""
Script to optimise campaigns for delivery, margin and CTR
Ad Ops should input required goals in the correct format in the description field on Fusion, script will parse
REQUIRES ABILITY TO MANIPULATE LINE ITEM BUDGETS, IF IO DICTATES WE NEED TO SPEND X ON A STRATEGY, SCRIPT WILL NEED THAT AS SEP CAMP


Need to take into account ad serving lines, remove budgets from campaign budget and throw them bitches away

COME UP WITH DATA BASED MODEL TO DO THIS SHIT, STOP MAKING ARBITRARY RULES

"""

activate_this = '/home/azeem.iqbal/Robomike/venv/bin/activate_this.py'
execfile(activate_this, dict(__file__=activate_this))

from datetime import date, timedelta, datetime, time
import numpy as np
import pandas as pd
import MySQLdb as mdb
import os
import json
import requests
import ast
import io
import copy
import smtplib
from email.mime.text import MIMEText
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication


abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)


#### TO DO: MAIN() AUTOMATICALLY CREATES NEW_CAMP_JSON AND PUSHES TO API
####        MAIN() AUTOMATICALLY CREATES CHILD CTR LINES IF DAY IS SUNDAY??


class FusionAPI:
    """To make doing API stuff easy later on"""
    def __init__(self):
        url = 'https://api.strikead.com/v1.1/login'

        with open('login.json') as file:
            login = json.load(file)
        file.close()

        r = requests.post(url, json=login)

        self.token = r.json()['token']

    def get_camp(self, campaign):
        url = "".join(['https://api.strikead.com/v1.1/campaigns/', str(campaign)])
        headers = {"Content-Type": "application/json", "X-Authorization": self.token}
        r = requests.get(url, headers=headers)

        return r.json()

    def put_camp(self, campaign, camp_json):
        url = "".join(['https://api.strikead.com/v1.1/campaigns/', str(campaign)])
        headers = {"Content-Type": "application/json", "X-Authorization": self.token}
        r = requests.put(url, headers=headers, json = camp_json)


def get_from_json(row, field, camp_json):
    line_json = [l for l in camp_json['line_items'] if l['id'] == int(row.line_item_id)]
    print line_json[0]['id']
    return line_json[0][field]


def read_campaigns():
    """
    Read txt doc to get list of camps to monitor
    """

    with open('campaigns.txt') as camps:
        list_of_camps = camps.read().splitlines()
    camps = []
    for camp in list_of_camps:
        camps.append(ast.literal_eval(camp))
    return camps


def make_table_name(date):
    """function to make GBQ table name from date"""
    d = date.strftime('%Y_%m_%d')
    table_name = "".join(['[fusion_production.slicer_', d, ']'])
    return table_name


def get_topline_delivery(campaign, camp_json):
    """Query GBQ to get delivery data for campaign, camp level stats of delivery CTR etc."""
    camp_start = datetime.strptime(camp_json['start_date'][:10], '%Y-%m-%d').date()
    camp_end = datetime.strptime(camp_json['end_date'][:10], '%Y-%m-%d').date()

    if camp_start == date.today():
        return pd.DataFrame()
    # Build query based on campaign start

    s = camp_start

    tables = []
    while s != min(camp_end + timedelta(1), date.today()):
        tables.append(make_table_name(s))
        s += timedelta(1)
    query_tables = ", ".join(tables)

    q_gbq = 'SELECT ad_id as line_item_id, date(timestamp) as date, sum(bids) as bids, sum(impressions) as imps, sum(media_spent_imp) as media_spend, sum(clicks) as clicks FROM '

    q = q_gbq + query_tables + ' WHERE campaign_id = ' + "".join(['"', campaign['ID'], '"']) + ' GROUP BY line_item_id, date'

    print q

    projID = 'strikead.com:strikeadbidderhosting'

    df = pd.read_gbq(q, projID)
    df['date'] = pd.to_datetime(df.date)
    df = df[df.date >= camp_start]
    df = df[df.date < date.today()]

    return df


def get_graphite_data(campaign, camp_json):

    # Get line items IDs from JSON, use to get selects
    # Get graphite stats - format of selects, bids, timestamp (every minute)

    now = datetime.utcnow() - timedelta(minutes = 1)
    camp_selects = {}

    # Iterate through lines in camp_json, get graphite data for that line, save to dict with keys of line item id, values of data

    for i in range(len(camp_json['line_items'])):
        params = {'target': ["".join(['sum.ad.', str(camp_json['line_items'][i]['id']), '.select']), "".join(['sum.ad.', str(camp_json['line_items'][i]['id']), '.bids'])],
                  'from': (date.today() - timedelta(5)).strftime('%Y%m%d'),
                  'until': (datetime.combine(date.today() - timedelta(1), time(23,59))).strftime('%H:%M_%Y%m%d'), 'format': 'json'}  # get 5 day window of selects/bids
        camp_selects[camp_json['line_items'][i]['id']] = {}

        data = requests.get('http://1.graphite.rtb2.p.va.us.strikead.com/render', params).json()

        try:
            camp_selects[camp_json['line_items'][i]['id']]['selects'] = data[0]['datapoints']
        except(KeyError, IndexError) as e:
            camp_selects[camp_json['line_items'][i]['id']]['selects'] = None

        try:
            camp_selects[camp_json['line_items'][i]['id']]['bids'] = data[1]['datapoints']
        except(KeyError, IndexError) as e:
            camp_selects[camp_json['line_items'][i]['id']]['bids'] = None



    # Create Pandas df from graphite data, fill in with 0s if no data
    # Trying to catch all cases of missing data from graphite so df is consistent

    df = None
    empty_lines = []
    for line in camp_selects:
        select_fail = 0
        bid_fail = 0

        if camp_selects[line]['selects']:
            temp_df_selects = pd.DataFrame(camp_selects[line]['selects']).rename(columns = {0: 'selects', 1: 'time'})
        else:
            select_fail = 1

        if camp_selects[line]['bids']:
            temp_df_bids = pd.DataFrame(camp_selects[line]['bids']).rename(columns = {0: 'bids', 1: 'time'})
        else:
            bid_fail = 1

        df_fail = 0

        if bid_fail == 1:
            if select_fail == 0:
                temp_df = temp_df_selects
                temp_df['bids'] = np.nan
                temp_df['line_item_id'] = line
            elif select_fail == 1:
                empty_lines.append(line)
                df_fail = 1
        else:
            temp_df = pd.merge(temp_df_bids, temp_df_selects, on = 'time', how = 'outer')
            temp_df['line_item_id'] = line
        if df_fail == 0:
            df = pd.concat([df, temp_df])

    # For some reason graphite returns select in QPS but bids in QPM

    df['selects'] = df.selects * 60
    df.fillna(0, inplace = True)

    # Add completely empty lines (no data at all) to df

    for line in empty_lines:
        line_df_temp = pd.DataFrame(df.time.unique())
        line_df_temp.rename(columns = {0: 'time'}, inplace = True)
        line_df_temp['bids'] = 0
        line_df_temp['selects'] = 0
        line_df_temp['line_item_id'] = line
        df = pd.concat([df, line_df_temp])

    df['time'] = pd.to_datetime(df.time, unit = 's')

    return df


def make_scores(campaign, camp_json, delivery, df_bidscore):
    """Delivery check, return % delivered DONT TOUCH"""
    def get_margin(row):
        line_json = [l for l in camp_json['line_items'] if l['id'] == int(row.line_item_id)]
        return 1 - (row.CPM / float(line_json[0]['billable_unit_price']))

    # Check historical delivery, see if under/over pacing

    df_bidscore = df_bidscore.groupby('line_item_id').agg(sum).reset_index()
    df_bidscore['bidrate'] = df_bidscore.bids / df_bidscore.selects
    df_bidscore['CPM'] = df_bidscore.media_spend / df_bidscore.imps * 1000
    df_bidscore['selectperc'] = df_bidscore.selects / df_bidscore.selects.sum()

    df_bidscore['max_bid'] = df_bidscore.apply(get_from_json, args = ('cpm_max_bid', camp_json), axis = 1)
    df_bidscore['billable'] = df_bidscore.apply(get_from_json, args = ('billable_unit_price', camp_json), axis = 1)

    df_bidscore['winrate'] = df_bidscore.imps / df_bidscore.bids
    df_bidscore['margin'] = df_bidscore.apply(get_margin, axis = 1)
    df_bidscore['CTR'] = df_bidscore.clicks / df_bidscore.imps

    df_bidscore['trafficscore'] = 2 * df_bidscore.selects - df_bidscore.bids
    df_bidscore['trafficscore'] = df_bidscore.trafficscore / df_bidscore.trafficscore.sum()
    df_bidscore['marginscore'] = (df_bidscore.margin)**2 * df_bidscore.trafficscore
    df_bidscore['marginscore'] = df_bidscore.marginscore / df_bidscore.marginscore.sum()
    df_bidscore['clickscore'] = (df_bidscore.CTR)**2 * df_bidscore.trafficscore
    df_bidscore['clickscore'] = df_bidscore.clickscore / df_bidscore.clickscore.sum()

    df_bidscore['score'] = (campaign['balance_coeff'] * df_bidscore.marginscore) + ((1 - campaign['balance_coeff']) * df_bidscore.clickscore)
    df_bidscore['score'] = df_bidscore.score / df_bidscore.score.sum()

    # Calculate new_required_daily_imps for campaign and use to calc wanted_imps per line

    return df_bidscore


def optimise(campaign, camp_json, delivery, df_bidscore):
    """Optimise campaign, balance for CTR/margin, balance_coeff = 1 --> Pure margin, 0 --> Pure CTR"""
    def recommend_line_budget(df_bidscore, pacing):
        if pacing < 1:
            df_bidscore['line_item_budget'] = df_bidscore.trafficscore * camp_json['budget']
        else:
            df_bidscore['line_item_budget'] = df_bidscore.score * camp_json['budget']

        return df_bidscore


    def recommend_max_bid(row, pacing):
        """Create bracket groups according to win/bidrates and recommend % increases based on group"""
        if pacing < 1:
            if row.winrate < 0.9:
                if row.winrate > 0.5:
                    return row.max_bid + (row.billable - row.max_bid) * 0.2
                if row.winrate > 0.4:
                    return row.max_bid + (row.billable - row.max_bid) * 0.3
                if row.winrate > 0.3:
                    return row.max_bid + (row.billable - row.max_bid) * 0.4
                return row.max_bid + (row.billable - row.max_bid) * 0.5
            else:
                return row.max_bid
        elif pacing > 1.05:
            if row.winrate > 0.7:
                return row.CPM + (row.max_bid - row.CPM) * 0.7
            if row.winrate > 0.5:
                return row.CPM + (row.max_bid - row.CPM) * 0.5
            if row.winrate > 0.2:
                return row.CPM + (row.max_bid - row.CPM) * 0.3
        else:
            return row.max_bid


    def make_json_from_df(row, optimised_json):
        for p in range(len(optimised_json['line_items'])):
            if optimised_json['line_items'][p]['id'] == int(row.line_item_id):
                if np.isnan(row.line_item_budget):
                    optimised_json['line_items'][p]['budget'] = None
                    optimised_json['line_items'][p]['pacing'] = None
                else:
                    optimised_json['line_items'][p]['budget'] = row.line_item_budget
                    optimised_json['line_items'][p]['pacing'] = row.pacing
                    optimised_json['line_items'][p]['cpm_max_bid'] = row.new_max_bid


    camp_start = datetime.strptime(camp_json['start_date'], '%Y-%m-%d %H:%M')
    camp_end = datetime.strptime(camp_json['end_date'], '%Y-%m-%d %H:%M')
    if camp_end.hour == 23 and camp_end.minute == 59:
        camp_end += timedelta(1) # Adding a day to count end date as a full day if autopopulated as last minute of the day
    today = date.today()
    flight_length = camp_end.date() - today
    days_live = today - camp_start.date()
    required_daily_imps = campaign['Impression Goal'] / float((camp_end.date() - camp_start.date()).days)
    camp_pacing = (delivery.imps.sum() / days_live.days) / required_daily_imps

    if delivery.empty:
        new_required_daily_imps = campaign['Impression Goal'] / float(flight_length.days)
    else:
        imps_required = campaign['Impression Goal'] - delivery.imps.sum()
        new_required_daily_imps = imps_required / float(flight_length.days)

    if camp_pacing < 1:
        df_bidscore['wanted_imps'] = new_required_daily_imps * df_bidscore.trafficscore
    else:
        df_bidscore['wanted_imps'] = new_required_daily_imps * df_bidscore.score

    # Use scores to calc new values

    df_bidscore['pacing'] = 'evenly'

    if camp_pacing < 0.9: # If low bidrate, switch pacing to ASAP
        df_bidscore['pacing'] = 'ASAP'

    df_bidscore['new_max_bid'] = df_bidscore.apply(recommend_max_bid, args = (camp_pacing,), axis = 1)
    df_bidscore['daily_cap'] = (df_bidscore.billable * df_bidscore.wanted_imps) / 1000

    if camp_pacing < 1: # Adjusting calculated daily caps for real world performance cuz Fusion
        df_bidscore['daily_cap'] = df_bidscore.daily_cap * 2.5
    else:
        df_bidscore['daily_cap'] = df_bidscore.apply(lambda row: row.daily_cap * 2.5 if row.daily_cap < 30 else row.daily_cap * 1.25, axis = 1)

    df_bidscore.ix[df_bidscore.new_max_bid == -1, 'new_max_bid'] = 'No change'
    df_bidscore = recommend_line_budget(df_bidscore, camp_pacing)
    df_bidscore['camp_pacing'] = camp_pacing

    optimised_json = copy.deepcopy(camp_json)

    if camp_pacing < 0.9:
        optimised_json['pacing'] = 'ASAP'

    # Sense check silly values, fusion cant handle tiny amounts of $
    df_bidscore.ix[df_bidscore.daily_cap < 1, 'daily_cap'] = 1
    df_bidscore.ix[df_bidscore.line_item_budget < 10, 'line_item_budget'] = np.NaN


    df_bidscore.apply(make_json_from_df, axis = 1, args = (optimised_json,))

    return df_bidscore, optimised_json


def check_camp(campaign):
    """
    Check margin, delivery, CTR of camp, output dict of values
    """
    # Get campaign data from JSON, remove ad serving lines

    camp_json = api.get_camp(campaign['ID'])

    ad_serving_lines = []

    for line in camp_json['line_items']:
        if line['buying_type'] == None:
            print line['id']
            ad_serving_lines.append(str(line['id']))
            ad_serving_lines.append(line['id'])

    # Get delivery from GBQ, selects from graphite, merge

    delivery = get_topline_delivery(campaign, camp_json)
    delivery = delivery[~delivery.line_item_id.isin(ad_serving_lines)]

    graphite_data = get_graphite_data(campaign, camp_json)
    graphite_data = graphite_data[~graphite_data.line_item_id.isin(ad_serving_lines)]

    gra = graphite_data.set_index('time').groupby([pd.TimeGrouper('D'), 'line_item_id']).agg(sum).reset_index().rename(columns = {'time': 'date'})
    gra['line_item_id'] = gra.line_item_id.astype(str)
    df = pd.merge(gra, delivery.drop('bids', axis = 1), on = ['date', 'line_item_id'], how = 'left')
    df.fillna(0, inplace = True)

    for p in range(1,6):
        df[df.date == date.today() - timedelta(p)][['bids', 'selects']].multiply(1 - p*0.16)

    df_bidscore = make_scores(campaign, camp_json, delivery, df)
    df_bidscore, optimised_json = optimise(campaign, camp_json, delivery, df_bidscore)

    #### GO FROM DF TO JSON

    return df_bidscore, camp_json


def writex(df):
    list_of_ids = df.apply(lambda row: write_to_txt(row), axis = 1)


def write_to_txt(row):
    line_temp = ','.join([row.ID, 'car2'])
    line_temp2 = ':'.join([line_temp, str(567), str(43829)])
    line_temp3 = '='.join([line_temp2, str(row.type)])
    temp_list.append(line_temp3)


def send_email(df, camp_json):
    message = MIMEMultipart()
    message['From'] = 'azeem.iqbal@strikead.com'
    message['To'] = camp_json['primary_contact']
    message['Subject'] = ' '.join(('Robomike recommendation for', date.strftime(date.today(), '%Y_%m_%d')))

    message_text = 'See attachment'
    message.attach(MIMEText(message_text, 'html'))

    message.attach(MIMEApplication(
    open("".join([str(camp_json['id']), '.csv']), 'rb').read(),
    Content_Disposition = "".join(['attachment; filename=Robomike_', str(camp_json['id']), date.strftime(date.today(), '%Y_%m_%d'), '.csv']),
    Name="".join(['attachment; filename=Robomike_', str(camp_json['id']), date.strftime(date.today(), '%Y_%m_%d'), '.csv'])
    ))



    try:
        server = smtplib.SMTP("smtp.gmail.com", 587)
        server.ehlo()
        server.starttls()
        server.login('azeem.iqbal@strikead.com', 'thisisapassword')
        server.sendmail('azeem.iqbal@strikead.com', camp_json['primary_contact'], message.as_string())

        server.close()
    except:
        print "failed to send mail"



def main():
    """
    Read list of campaigns to monitor from text file, check how they are doing then change accordingly
    """

    list_of_campaigns = read_campaigns()

    global api
    api = FusionAPI()


    # Once txt file is read giving script list of camps to monitor, check how each camp is doing and save status to
    # dict for later. Status should say CTR OK or not OK and values
    campaign_statuses = {}
    for campaign in list_of_campaigns:
        campaign_statuses[campaign['ID']] = check_camp(campaign)

    for k in campaign_statuses.keys():
        df = campaign_statuses[k][0]
        df.to_csv("".join([str(k), '.csv']), index = False)
        send_email(df, campaign_statuses[k][1])

    #### code to convert df to edited JSON, use this json for create_ctr_lines too




    # Act on status always aim for 30 min margin



if __name__ == "__main__":
    main()